package test5;
import java.util.Scanner;

class ArrayUtils
{

    PairResult findMinMax(double[] values)
    {
        PairResult pair=new PairResult(values[0],values[0]);
        for(int i=1;i<values.length;i++)
        {
            if(values[i]>pair.max)
                pair.max=values[i];
        }
        for(int i=1;i<values.length;i++)
        {
            if(values[i]<pair.min)
                pair.min=values[i];
        }
        System.out.println(pair.toString());
        return pair;

    }



    class PairResult
    {
        private double min;
        private double max;

        public PairResult(double min, double max) {
            this.min = min;
            this.max = max;
        }

        @Override
        public String toString() {
            return "PairResult [" +
                    "min=" + min +
                    ", max=" + max +
                    ']';
        }
    }
}
public class test5_4 {
    public static void main(String[] args)
    {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        ArrayUtils u=new ArrayUtils();
        double[] num=new double[n];
        for(int i=0;i<n;i++)
        {
            num[i]=sc.nextDouble();
        }
        u.findMinMax(num);
        System.out.println(ArrayUtils.PairResult.class);
    }
}