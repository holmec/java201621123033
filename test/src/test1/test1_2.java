package test1;
import java.util.Scanner;
import java.lang.Integer;
public class test1_2 {
	public static void main(String[] args){
		Scanner sr=new Scanner(System.in);
		while(true){
			String str=sr.nextLine();
			int num=Integer.parseInt(str);
			int[] array=new int[str.length()];
			int sum=0,i,a,j,length;
			if(num>=10000&&num<=20000){
				System.out.print(Integer.toBinaryString(num)+",");
				System.out.print(Integer.toOctalString(num)+",");
				System.out.print(Integer.toHexString(num)+"\n");
			}
			else{
				if(num<0)
					length=str.length()-1;
				else
					length=str.length();
				num=Math.abs(num);
				for(i=0;i<length;i++){
					a=num%10;
					array[i]=a;
					num=num/10;
					sum=sum+a;
				}
				for(j=i-1;j>=0;j--)
					System.out.print(array[j]+" ");
				System.out.println(sum);
			}
		}
		
	}

}
