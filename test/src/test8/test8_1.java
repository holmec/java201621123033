package test8;

import java.util.ArrayList;
import java.util.Scanner;

class ArrayListIntegerStack implements IntegerStack{
	
	ArrayList<Integer> list=new ArrayList<>();
	

	@Override
	public Integer push(Integer item) {
		if(item==null)
		    return null;
		else{
			list.add(item);
		}
			return item;
			
	}

	@Override
	public Integer pop() {
		if(list.size()==0)
		return null;
		else{
			return list.remove(list.size()-1);
		}
		
	}

	@Override
	public Integer peek() {
		if(list.size()==0)
			return null;
		else
		return list.get(list.size()-1);
	}

	@Override
	public boolean empty() {
		if(list.size()==0)
			return true;
		else
		return false;
	}

	@Override
	public int size() {
		return list.size();
	}
	

	@Override
	public String toString() {
		return "" + list + "";
	}


}

public class test8_1{

	public static void main(String[] args){
		ArrayListIntegerStack stack=new ArrayListIntegerStack();
		Scanner sr=new Scanner(System.in);
		int m=sr.nextInt();
		for(int i=0;i<m;i++){
			//stack.push(sr.nextInt());
			System.out.println(stack.push(sr.nextInt()));
		}
		System.out.println(stack.peek()+","+stack.empty()+","+stack.size());
		System.out.println(stack.toString());
		int x=sr.nextInt();
		int size=stack.size();
		if(x<stack.size()){
		for(int i=0;i<x;i++){
			System.out.println(stack.pop());
		}
		}
		else{
			for(int i=0;i<size;i++){
				System.out.println(stack.pop());
			}
		}
		System.out.println(stack.peek()+","+stack.empty()+","+stack.size());
		System.out.println(stack.toString());
	}
}



