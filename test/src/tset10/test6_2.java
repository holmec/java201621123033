package tset10;

import java.util.Scanner;

class Resource{
	void open(String str){
		
	}
	
	void close(){
		
	}
}

public class test6_2 {
	public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
	    Resource resource = null;
	    try{
	        resource = new Resource();
	        resource.open(sc.nextLine());
	        System.out.println("resource open success");
	    }catch(Exception e){
	    	System.out.println(e);
	    }finally{
	    	try{
	    		resource.close();
		        System.out.println("resource release success");
	    	}catch(Exception e){
	    		System.out.println(e);
	    	}
	    }
	       sc.close();

	}

}
