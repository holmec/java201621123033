package test11;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Account1{
	private int balance;
	
	private static Lock lock=new ReentrantLock();
	
	public Account1(int balance) {
		this.balance = balance;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public void deposit(int money){
		lock.lock();
		balance=balance+money;
		lock.unlock();
	}
	
	public synchronized void withdraw(int money){
		lock.lock();
		balance=balance-money;
		if(balance<0) 
	        throw new IllegalStateException(balance+"");	
		lock.unlock();
    }

}


public class test6_5 {

}
