package tset10;


import java.util.Scanner;

class ArrayUtils{
	public static double findMax(double[] arr,int begin, int end) throws IllegalArgumentException{
		try{
			double max=arr[0];

			if(begin>=end){
				throw new IllegalArgumentException("begin:"+begin+" >= end:"+end);
			}
			else if(begin<0){
				throw new IllegalArgumentException("begin:"+begin+" < 0");
			}
			else if(end>arr.length){
				throw new IllegalArgumentException("end:"+end+" > arr.length");
			}
			for(int i=begin;i<end;i++){
				if(arr[i]>max)
					max=arr[i];
			}
			System.out.println(max);
		}catch(Exception e){
			System.out.println(e);
		}
		return 0;
	}
}

public class test7_3{

	public static void main(String[] args) {
				Scanner sr=new Scanner(System.in);
				int n=sr.nextInt();
				int[] arr=new int[n];
				double[] darr=new double[n];
				for(int i=0;i<n;i++){
					int a=sr.nextInt();
					arr[i]=a;
					darr[i]=(double)arr[i];
				}
				
				while(sr.hasNextInt()){
					ArrayUtils.findMax(darr,sr.nextInt(),sr.nextInt());
				}	
		
		try {
			System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class,int.class,int.class));
		} catch (Exception e1) {
		} 
		
		
	}

}


