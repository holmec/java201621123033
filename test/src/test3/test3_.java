package test3;

import java.util.Scanner;

class Rational{
	private int v1;
	private int v2;
	
	public Rational(int v1,int v2){
		this.v1=v1;
		this.v2=v2;
	}
	
	public Rational(){
		
	}
	
	public Rational add(Rational T1,Rational T2){
		int v1=T1.v1*T2.v2+T1.v2*T2.v1;
		int v2=T1.v2*T2.v2;
	    int gcd=gcd(v1,v2);
	    v1=v1/gcd;
	    v2=v2/gcd;
		return new Rational(v1,v2);
	}
	
	public Rational multiply(Rational T1,Rational T2){
		int v1=T1.v1*T2.v1;
		int v2=T1.v2*T2.v2;
		int gcd=gcd(v1,v2);
	    v1=v1/gcd;
	    v2=v2/gcd;
		return new Rational(v1,v2);
	}
	
	public int gcd(int v1,int v2){
		int a;
		while(v1%v2!=0){
			a=v2;
			v2=v1%v2;
			v1=a;
		}
		return v2;
	}
	
	public int GetRational(Rational T,int i,int e){
		if(i==1)
		e=T.v1;
		else
		e=T.v2;
		return e;
	}

	@Override
	public String toString() {
		return "Rational [v1=" + v1 + ", v2=" + v2 + "]";
	}
	
}

public class test3_ {
	public static void main(String[] args){
		Scanner sr=new Scanner(System.in);
		int i,e=0;
		Rational T1=new Rational(sr.nextInt(),sr.nextInt());
		Rational T2=new Rational(sr.nextInt(),sr.nextInt());
		Rational T3=new Rational();
		System.out.println(T3.add(T1,T2).toString());
		System.out.println(T3.multiply(T1,T2).toString());
		T3=T3.multiply(T1, T2);
		i=sr.nextInt();
		e=T3.GetRational(T3,i,e);
		System.out.println(e);
		System.out.println("201621123033 Ҧ����");
	}


	
	

}
