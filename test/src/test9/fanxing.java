package test9;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

interface GeneralStack<E> {
	E push(E item);           
	E pop();                
	E peek();                
	public boolean empty();
	public int size();     

}

class Car {
	private int id;
	private String name;
	
	public Car(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Car [id=" + id + ", name=" + name + "]";
	}
	

}

class ArrayListGeneralStack<E> implements GeneralStack<E>{
	private List<E> list=new ArrayList<E>();

	@Override
	public E push(E item) {
		if(item==null)
		return null;
		else
		{
			list.add(item);
		}
		return item;
	}

	@Override
	public E pop() {
		if(list.size()==0)
			return null;
			else{
				return list.remove(list.size()-1);
			}
	}

	@Override
	public E peek() {
		if(list.size()==0)
			return null;
		else
		return list.get(list.size()-1);
	}

	@Override
	public boolean empty() {
		if(list.size()==0)
			return true;
		else
		return false;
	}

	@Override
	public int size() {
		return list.size();
	}
		
	@Override
	public String toString() {
		return "" + list + "";
	}

	
	
}

public class fanxing{
	public static void main(String[] args){
		Scanner sr=new Scanner(System.in);
		
		
		while(true){
			String swi=sr.next();
			int m=sr.nextInt();
			int n=sr.nextInt();
			ArrayListGeneralStack<Object> stack=new ArrayListGeneralStack<Object>();
			switch(swi){
			case "quit":
				System.exit(0);
			case "Integer":
				int sum=0;				
				System.out.println("Integer Test");
				for(int i=0;i<m;i++){
					int a=sr.nextInt();
					System.out.println("push:"+stack.push(a));				
				}
				for(int i=0;i<n;i++){
					if(stack.peek()==null)break;
					System.out.println("pop:"+stack.pop());
				}
				System.out.println(stack.toString());
				
				while(!stack.empty())
                {
					sum=sum+(Integer)stack.pop();
                }
				System.out.println("sum="+sum);
				System.out.println(stack.getClass().getInterfaces()[0]);
				break;
			case "Double":			
				double sum1=0.0;
				System.out.println("Double Test");
				for(int i=0;i<m;i++){
					double a=sr.nextDouble();
					System.out.println("push:"+stack.push(a));				
				}
				for(int i=0;i<n;i++){
					if(stack.peek()==null)break;
					System.out.println("pop:"+stack.pop());
				}
				System.out.println(stack.toString());
				while(!stack.empty())
                {
					sum1=sum1+(Double)stack.pop();
                }
				System.out.println("sum="+sum1);
				System.out.println(stack.getClass().getInterfaces()[0]);
				break;
			case "Car":
				System.out.println("Car Test");
				for(int i=0;i<m;i++){
					Car a=new Car(sr.nextInt(),sr.next());
					System.out.println("push:"+stack.push(a));				
				}
				for(int i=0;i<n;i++){
					if(stack.peek()==null)break;
					System.out.println("pop:"+stack.pop());
				}
				System.out.println(stack.toString());
				while(!stack.empty()){
					Car b=(Car)stack.pop();
					System.out.println(b.getName());
				}
				System.out.println(stack.getClass().getInterfaces()[0]);
				break;
			}
				
			}
		}
		
}



