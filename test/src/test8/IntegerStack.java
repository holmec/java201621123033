package test8;

public interface IntegerStack {
	public Integer push(Integer item); //如item为null，则不入栈直接返回null。否则直接入栈，然后返回item。
	public Integer pop();              //出栈，如栈为空，则返回null。
	public Integer peek();             //获得栈顶元素，如栈顶为空，则返回null。注意：不要出栈
	public boolean empty();           //如过栈为空返回true
	public int size();                //返回栈中元素数量

}
