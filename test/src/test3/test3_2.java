package test3;

import java.util.Scanner;
import java.lang.Integer;

class Persons {
	private String name;
	private int age;
	private boolean gender;
	private int id;
	static int count=0;
	
	static{
		System.out.println("This is static initialization block");
	}
	
	{
		id=count;
		System.out.println("This is initialization block, id is "+id);
	}

	public Persons() {
		System.out.println("This is constructor");
		System.out.printf("%s,%d,%s,%d\n", name, age, gender, id);
	}

	public Persons(String name, int age,boolean gender) {
		this.name = name;
		this.gender = gender;
		this.age = age;
		count++;
	}

	public void SetName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
	}
	
}

public class test3_2 {
	public static void main(String[] args){
		Scanner sr = new Scanner(System.in);
		int n = Integer.parseInt(sr.nextLine());
		int i,j;
		Persons[] p = new Persons[n];
		for (i = 0; i < p.length; i++) {
			Persons person=new Persons(sr.next(),sr.nextInt(),sr.nextBoolean());
			p[i]=person;
		}
		for(j=p.length-1;j>=0;j--){
			System.out.println(p[j]);
		}
		Persons pp=new Persons();
		System.out.println(pp);
		sr.close();
	}

}
