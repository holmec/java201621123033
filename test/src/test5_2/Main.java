package test5_2;

import java.util.Arrays;
import java.util.Scanner;

class PersonSortable{
	String name;
	int age;
	
	public PersonSortable(String name,int age){
		this.name=name;
		this.age=age;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return name + "-" + age ;
	}
	
	
}

public class Main {
	public static void main(String[] args){
		Scanner sr=new Scanner(System.in);
		int n=sr.nextInt();
		PersonSortable[] person=new PersonSortable[n];
		for(int i=0;i<n;i++){
			person[i]=new PersonSortable(sr.next(),sr.nextInt());
		}
		System.out.println("201621123033,Ҧ����");
		System.out.println("NameComparator:sort");
		Arrays.sort(person,(PersonSortable o1,PersonSortable o2)->o1.getName().compareTo(o2.getName()));
		for(int i=0;i<n;i++){
			System.out.println(person[i]);
		}
		System.out.println("AgeComparator:sort");
		Arrays.sort(person,(PersonSortable o1,PersonSortable o2)->o1.getAge()-o2.getAge());
		for(int i=0;i<n;i++){
			System.out.println(person[i]);
		}
		
		System.out.println(Arrays.toString(NameComparetor.class.getInterfaces()));
		System.out.println(Arrays.toString(AgeComparetor.class.getInterfaces()));
	}

}
