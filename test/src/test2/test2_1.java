package test2;

import java.util.Arrays;
import java.util.Scanner;
import java.lang.Integer;
public class test2_1 {
	public static void main(String[] args){
		Scanner sr=new Scanner(System.in);
		String[] Digit=null;
		int[] digit=null;
		while(true){
		String choice=sr.nextLine();
		switch(choice){
		case "fib":
			int n= Integer.parseInt(sr.nextLine());
			fib(n);
			System.out.print("\n");
			break;
		case "sort":
			String a =sr.nextLine();
			Digit = a.split(" ");
			digit=converToIntDigit(Digit);
			Arrays.sort(digit);
			System.out.println(Arrays.toString(digit));
			break;	
		case "search":
			Arrays.sort(digit);
			int index=Integer.parseInt(sr.nextLine());
			int p=Arrays.binarySearch(digit,index);
			if(p<0)
				System.out.println(-1);
			else
				System.out.println(p);
			break;
		case "getBirthDate":
			int m=Integer.parseInt(sr.nextLine());
			int i;
			//System.out.println(m);
			for(i=0;i<m;i++){
				String id=sr.nextLine();
				String year=id.substring(6,10);
				String month=id.substring(10,12);
				String day=id.substring(12,14);
				System.out.println(year+"-"+month+"-"+day);
			}
			break;
		default:
		   System.out.println("exit");
		   break;
		}
		}
	}
	public static void fib(int n){
		int a=1,b=1,c=0;
		int i;
		if(n==1){
			System.out.print(a);
		}
		else if(n==2){
			System.out.print(a+" "+b);
		}
		else{
		System.out.print(a+" "+b);
		for(i=0;i<n-2;i++){
			c=a+b;
			a=b;
			b=c;
			System.out.print(" "+c);
		}
		}
	}
	public static int[] converToIntDigit(String[] Digit){
		int i;
		int[] digit=new int[Digit.length];
		for(i=0;i<Digit.length;i++){
			digit[i]=Integer.parseInt(Digit[i]);
		}
		return digit;
	}

}
