package test4;

class Person{
	private String name;
	private int age;
	private boolean gender;
	
	public Person(String name, int age, boolean gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}

	@Override
	public String toString() {
		return  name + "-" + age + "-" + gender ;
	}

}

class Company{
	private String name;

	public Company(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

}

class Employee extends Person{
	public Employee(String name, int age, boolean gender) {
		super(name, age, gender);
		// TODO Auto-generated constructor stub
	}
	
	private Company company;
	private double salary;
	@Override
	public String toString() {
		return super.toString() + company + "-" + salary ;
	}
	
}

public class test4_h {

}
