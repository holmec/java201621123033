package test4;

public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (!super.equals(obj))
		return false;
	if (getClass() != obj.getClass())
		return false;
	Employee other = (Employee) obj;
	if (company == null) {
		if (other.company != null)
			return false;
	} else if (!company.equals(other.company))
		return false;
	DecimalFormat df = new DecimalFormat("#.##");
	String a = df.format(this.salary);
	double a1 = Double.parseDouble(a);
	String b = df.format(other.salary);
	double b1 = Double.parseDouble(b);
	if (a1 != b1)
		return false;
	return true;
}


public class test4_6_3 {
	

}
