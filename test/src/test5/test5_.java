package test5;

import java.util.Scanner;
import java.lang.Integer;
import java.util.Arrays;

abstract class Shape implements Comparable<Shape>{
	final static double PI = 3.14;

	public abstract double getPerimeter();

	public abstract double getArea();

	@Override
	public int compareTo(Shape o) {
		if(this.getArea()-o.getArea()<0)
			return -1;
		else if(this.getArea()-o.getArea()>0)
			return 1;
		return 0;
	}
	
	
}


class Rectangle extends Shape{
	private int width,length;
	
	public Rectangle(int width, int length){
		this.width=width;
		this.length=length;
	}
	
	public double getPerimeter(){
		return (2*width+2*length);
	}
	
	public double getArea(){
		return (width*length);
	}

	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}
	
}

class Circle extends Shape{
	private int radius;
	
	public Circle(int radius){
		this.radius=radius;
	}
	
	public double getPerimeter(){
		return (2*PI*radius);
	}
	
	public double getArea(){
		return (PI*radius*radius);
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	
	
}

public class test5_ {
	public static double sumAllArea(Shape[] shape){
		double sum=0;
		for(Shape a:shape){
			sum+=a.getArea();
		}
		return sum;
	}
	
	public static double sumAllPerimeter(Shape[] shape){
		double sum=0;
		for(Shape b:shape){
			sum+=b.getPerimeter();
		}
		return sum;
	}
	
	public static void main(String[] args){
		Scanner sr=new Scanner(System.in);
		/*Rectangle[] r=new Rectangle[2];
		Circle[] c=new Circle[2];*/
		int n=Integer.parseInt(sr.nextLine());
		Shape[] shape=new Shape[n];
		for(int i=0;i<shape.length;i++){
			String j=sr.nextLine();
			if(j.equals("rect")){
				Rectangle r=new Rectangle(sr.nextInt(),sr.nextInt()); 
				sr.nextLine();
				shape[i]=r;
			}
			else if(j.equals("cir")){
				Circle c=new Circle(sr.nextInt()); 
				sr.nextLine();
				shape[i]=c;
			}
		}
		/*System.out.println(sumAllPerimeter(shape));
		System.out.println(sumAllArea(shape));
		System.out.println(Arrays.deepToString(shape));
		for(Shape c:shape){
			System.out.println(c.getClass()+","+c.getClass().getSuperclass());
		}*/
		Arrays.sort(shape);
		System.out.println("������������У�");
		for(int i=0;i<shape.length;i++){
			System.out.println(shape[i]);
		}
		
	}

}



