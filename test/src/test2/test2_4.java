package test2;

import java.util.Arrays;
import java.util.Scanner;
import java.lang.Integer;
public class test2_4 {
	public static void main(String[] args){
		Scanner sr=new Scanner(System.in);

		while(true){
			int n=sr.nextInt();
			String[][] arrays=new String[n][];
			int i,j;
			int m;
			for(i=0;i<n;i++){
				arrays[i]=new String[i+1];
			}
			for(i=1;i<=n;i++){
				for(j=1;j<=i;j++){
					m=i*j;
					String a=Integer.toString(i);
					String b=Integer.toString(j);
					String c=Integer.toString(m);
					arrays[i-1][j-1]=a+"*"+b+"="+c;
					if(j<i){
						System.out.printf("%-7s",arrays[i-1][j-1]);
					}
					else if(j==i){
						System.out.printf("%s\n",arrays[i-1][j-1]);

					}
				}
			}
			System.out.println(Arrays.deepToString(arrays));
		}
	}

}
