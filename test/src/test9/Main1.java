package test9;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

interface GeneralStack1<T>{
    T push(T item);
    T pop();
    T peek();
    public boolean empty();
    public int size();
}

class ArrayListGeneralStack1<T> implements GeneralStack1<T>{

    private List<T> list = new ArrayList<>();

    @Override
    public T push(T item) {
        if(item == null)
            return null;
        else{
            list.add(item);
            return item;
        }
    }
    @Override
    public T pop() {
        if(list.size() == 0)
            return null;
        else
            return list.remove(list.size()-1);
    }
    @Override
    public T peek() {
        if(list.size() == 0)
            return null;
        else
            return list.get(list.size()-1);
    }
    @Override
    public boolean empty() {
        return list.isEmpty();
    }
    @Override
    public int size() {
        return list.size();
    }
    @Override
    public String toString() {
        return list.toString();
    }
}

class Car1{
    private int id;
    private String name;


    public Car1(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return "Car [id=" + id + ", name=" + name + "]";
    }

}

public class Main1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while(in.hasNext()){
            String choose = in.next();
            if(choose.equals("quit"))
                System.exit(0);
            else{
                int m = in.nextInt(),n = in.nextInt();
                ArrayListGeneralStack stack;
                switch (choose) {
                    case "Integer":
                        System.out.println("Integer Test");
                        stack = new ArrayListGeneralStack<Integer>();
                        for (int i = 0; i < m; i++)
                            System.out.println("push:"+stack.push(in.nextInt()));
                        for (int i = 0; i < n; i++)
                            System.out.println("pop:"+stack.pop());
                        System.out.println(stack);
                        int sum = 0;
                        for (int i = 0; i < m-n ; i++) {
                            sum = sum + (Integer) stack.peek();
                            stack.pop();
                        }
                        /*while(!stack.empty()){
                            int num = (Integer) stack.pop();
                            sum += num;
                        }*/
                        System.out.println("sum="+sum);
                        System.out.println(stack.getClass().getInterfaces()[0]);
                        break;
                    case "Double":
                        System.out.println("Double Test");
                        stack = new ArrayListGeneralStack<Double>();
                        for (int i = 0; i < m; i++)
                            System.out.println("push:"+stack.push(in.nextDouble()));
                        for (int i = 0; i < n; i++)
                            System.out.println("pop:"+stack.pop());
                        System.out.println(stack);
                        double sum1 = 0.0;
                        for (int i = 0; i < m-n ; i++) {
                            sum1 = sum1 + (Double) stack.peek();
                            stack.pop();
                        }
                        /*while(!stack.empty()){
                            double num1 = (double)stack.pop();
                            sum1 += num1;
                        }*/
                        System.out.println("sum="+sum1);
                        System.out.println(stack.getClass().getInterfaces()[0]);
                        break;
                    case "Car":
                        System.out.println("Car Test");
                        stack = new ArrayListGeneralStack<Car>();
                        for (int i = 0; i < m; i++) {
                            Car1 car = new Car1(in.nextInt(),in.next());
                            System.out.println("push:"+stack.push(car));
                        }
                        for (int i = 0; i < n; i++)
                            System.out.println("pop:"+stack.pop());
                        List<Car1> cars = new ArrayList<Car1>();
                        for (int i = 0; i < m-n ; i++) {
                            Car1 car1 = (Car1)stack.peek();
                            System.out.println(car1.getName());
                            stack.pop();
                        }
                        /*while(!stack.empty()){
                            Car car1 = (Car)stack.pop();
                            cars.add(car1);
                            System.out.println(cars);
                        }*/
                        for (int i = 0; i < cars.size(); i++)
                            System.out.println(cars.get(i).getName());
                        System.out.println(stack.getClass().getInterfaces()[0]);
                        break;
                }
            }
        }
    }
}