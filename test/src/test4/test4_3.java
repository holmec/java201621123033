package test4;

import java.util.ArrayList;

class Fruit{
	private String name;
	public Fruit(String name){
		this.name=name;
	}
	@Override
	public String toString() {
		return name + "-"+super.toString();
	}
	
	public boolean equals(Object obj){
		if(this==obj)
			return true;
		if(obj==null)
			return false;
		if(name==null&&obj!=null){
			return false;
		}
		if(obj.getClass() != this.getClass())
			return false;
		Fruit l=(Fruit)obj;
		if(name.equalsIgnoreCase(l.name))
			return true;
		return false;

	}
	
}

public class test4_3 {
	public static void main(String[] args){
		ArrayList<Fruit> fruitlist=new ArrayList<Fruit>();
		Fruit[] fruit= new Fruit[3];
		fruit[0] = new Fruit("orange");
		fruit[1] = new Fruit("banana");
		fruit[2] = new Fruit("Banana");
		for(int i=0;i<fruit.length;i++){
            if(fruitlist.contains(fruit[i])==false)
                fruitlist.add(fruit[i]);
        }
        for(int i=0;i<fruitlist.size();i++)
            System.out.println(fruitlist.get(i));
    }
}


