package tset10;

import java.util.Arrays;
import java.util.Scanner;

public class test7_2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = Integer.parseInt(sc.nextLine());
		int[] arr = new int[n];
		for (int i = 0; i < arr.length;i++) {			
			String line = sc.nextLine();
			try{
				int value = Integer.parseInt(line);
				arr[i] = value;
			}catch(Exception e){
				System.out.println(e);
				i--;
			}
						
			
		}

		System.out.println(Arrays.toString(arr));
		sc.close();
	}

}