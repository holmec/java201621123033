package test11;

import java.util.Arrays;

public class test6_3 {	
    public static void main(String[] args) {
        final String mainThreadName = Thread.currentThread().getName();
        Thread t1 = new Thread(
        		()->{
        		System.out.println(mainThreadName);
        		System.out.println(Thread.currentThread().getName());
        		System.out.println(Arrays.toString(Thread.class.getInterfaces()));
        		}
   
        );
        t1.start();
    }
}
