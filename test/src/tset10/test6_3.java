package tset10;

import java.util.EmptyStackException;
import java.util.Stack;



interface IntegerStack {
	public Integer push(Integer item); 
	public Integer pop();              
	public Integer peek();            

}

class ArrayIntegerStack implements IntegerStack{
    private int capacity;
    private int top=0;
    private Integer[] arrStack;
    

	@Override
	public Integer push(Integer item) {
		if(item == null)
			return null;
		if(top==capacity){
			throw new FullStackException();
		}
		arrStack[top++]=item;
		return item;
	}
	@Override
	public Integer pop() {
		if(top==0)
			throw new EmptyStackException();
		top--;
		return arrStack[top];
	}
	@Override
	public Integer peek() {

		if(top==0)
			throw new EmptyStackException();
		return arrStack[top-1];
	}
	
	
}

public class test6_3 {

}
