package test11;

class Account{
	private int balance;
	
	public Account(int balance) {
		this.balance = balance;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	synchronized void deposit(int money){
		balance=balance+money;
	}
	
	synchronized void withdraw(int money){
		balance=balance-money;
	}
}

public class test6_4 {

}
