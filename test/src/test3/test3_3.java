package test3;

import java.util.Scanner;
import java.lang.Integer;
import java.util.Arrays;

class Rectangle{
	private int width,length;
	
	public Rectangle(int width, int length){
		this.width=width;
		this.length=length;
	}
	
	public int getPerimeter(){
		return (int)(2*width+2*length);
	}
	
	public int getArea(){
		return (int)(width*length);
	}

	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}
	
}

class Circle{
	private int radius;
	
	public Circle(int radius){
		this.radius=radius;
	}
	
	public int getPerimeter(){
		return (int)(2*Math.PI*radius);
	}
	
	public int getArea(){
		return (int)(Math.PI*radius*radius);
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	
	
}

public class test3_3 {
	public static void main(String[] args){
		Scanner sr=new Scanner(System.in);
		Rectangle[] r=new Rectangle[2];
		Circle[] c=new Circle[2];
		for(int i=0;i<2;i++){
			Rectangle rectangle=new Rectangle(sr.nextInt(),sr.nextInt());
			r[i]=rectangle;
		}
		for(int i=0;i<2;i++){
			Circle circle=new Circle(sr.nextInt());
			c[i]=circle;
		}
		int perimeter=0;
		int area=0;
		for(int j=r.length-1;j>=0;j--){
			
			perimeter=perimeter+r[j].getPerimeter()+c[j].getPerimeter();
		}
		for(int j=r.length-1;j>=0;j--){
			area=area+r[j].getArea()+c[j].getArea();
		}
		System.out.println(perimeter);
		System.out.println(area);
		System.out.println(Arrays.deepToString(r));
		System.out.println(Arrays.deepToString(c));
	}

}
