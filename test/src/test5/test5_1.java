package test5;
import java.util.Arrays;
import java.util.Scanner;

class PersonSortable implements Comparable<PersonSortable>{
	private String name;
	private int age;
	
	public PersonSortable(String name,int age){
		this.name=name;
		this.age=age;
	}
	
	@Override
	public int compareTo(PersonSortable o){
		if((name.compareTo(o.name))==0){
			if(age-o.age<0){
				return -1;
			}
			if(age-o.age>0)
				return 1;
			return 0;
		}
		
			
		return
			name.compareTo(o.name);
	}

	@Override
	public String toString() {
		return name + "-" + age ;
	}
	
	
}

public class test5_1 {
	public static void main(String[] args){
		Scanner sr=new Scanner(System.in);
		int n=sr.nextInt();
		PersonSortable[] person=new PersonSortable[n];
		for(int i=0;i<n;i++){
			person[i]=new PersonSortable(sr.next(),sr.nextInt());
		}
		Arrays.sort(person);
		for(int i=0;i<n;i++){
			System.out.println(person[i]);
		}
		System.out.println(Arrays.toString(PersonSortable.class.getInterfaces()));
	}

}
