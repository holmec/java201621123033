package test3;

import java.util.Scanner;
import java.lang.Integer;

class Person {
	private String name;
	private int age;
	private boolean gender;
	private int id;

	public Person() {
		System.out.println("This is constructor");
		System.out.printf("%s,%d,%s,%d\n", name, age, gender, id);
	}

	public Person(String name, int age,boolean gender) {
		this.name = name;
		this.gender = gender;
		this.age = age;
	}

	public void SetName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
	}
	
}

public class test3_1{
	public static void main(String[] agrs) {
		Scanner sr = new Scanner(System.in);
		int n = Integer.parseInt(sr.nextLine());
		int i,j;
		Person[] p = new Person[n];
		for (i = 0; i < p.length; i++) {
			Person person=new Person(sr.next(),sr.nextInt(),sr.nextBoolean());
			p[i]=person;
		}
		for(j=p.length-1;j>=0;j--){
			System.out.println(p[j]);
		}
		Person pp=new Person();
		System.out.println(pp);
		sr.close();
	}
}


