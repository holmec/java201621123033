package tset10;

import java.util.Scanner;

class IllegalScoreException extends Exception{
	IllegalScoreException(String msg){
		super(msg);
	}
}

class IllegalNameException extends Exception{
	IllegalNameException(String msg){
		super(msg);
	}
}

class Student{
	private String name;
	private int score;
	
	
	public Student(String name, int score) {

		this.name = name;
		this.score = score;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) throws IllegalNameException{
		
			if(name.charAt(0)>='0'&&name.charAt(0)<='9'){
				throw new IllegalNameException("the first char of name must not be digit, name="+name);
			}
			this.name = name;
		
		
	}
	public int getscore() {
		return score;
	}
	public void setSorce(int score) {
		this.score = score;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", score=" + score + "]";
	}
	public int addScore(int score) throws IllegalScoreException{
			if(score<0||score>100){
				throw new IllegalScoreException("score out of range, score="+score);
		
		}
		return score;
		
	}
}

public class test7_4 {
	public static void main(String[] args) throws Exception{
		Scanner sr=new Scanner(System.in);
		while(true){
			String choice=sr.next();
			if(choice.equals("new")){
				try{
					String name=sr.next();
					int score=sr.nextInt();
					Student student=new Student(name,score);
					student.setName(name);
					student.addScore(score);
					System.out.println(student.toString());
				}catch(IllegalScoreException e){
					System.out.println(e);
				}catch(IllegalNameException e){
					System.out.println(e);
				}catch(Exception e){
					System.out.println("java.util.NoSuchElementException");
				}
			}
			else{
				break;
			}
			
		}
		sr.close();
		System.out.println("scanner closed");
	}
	

}
