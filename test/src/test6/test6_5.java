package test6;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

class Person implements Comparable<Person>{
	private String name;
	private int age;
	private boolean gender;
	public Person(String name, int age, boolean gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
    
	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public boolean isGender() {
		return gender;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public String toString() {
		return name+'-'+age+'-'+gender;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (age != other.age)
			return false;
		if (gender != other.gender)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(Person o) {
		if(this.name.equals(o.name)){
			if(this.age>o.age)
				return 1;
			else if(this.age<o.age)
				return -1;
			else return 0;
		}
		else{
			return this.name.compareTo(o.name);
		}
			
		
	}
	
}

class Student extends Person{
	private String stuNo;
	private String clazz;
	public Student(String name, int age, boolean gender, String stuNo, String clazz) {
		super(name, age, gender);
		this.stuNo = stuNo;
		this.clazz = clazz;
	}
	public String toString() {
		return "Student:"+super.toString()+"-"+stuNo+"-"+clazz;
	}
	
	public String getStuNo() {
		return stuNo;
	}
	
	public String getClazz() {
		return clazz;
	}
	
	public void setStuNo(String stuNo) {
		this.stuNo = stuNo;
	}
	
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (clazz == null) {
			if (other.clazz != null)
				return false;
		} else if (!clazz.equals(other.clazz))
			return false;
		if (stuNo == null) {
			if (other.stuNo != null)
				return false;
		} else if (!stuNo.equals(other.stuNo))
			return false;
		return true;
	}
	
	
}

class Company{
	String name;

	public Company(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
class Employee extends Person{
	private double salary;
	private Company company;
	public Employee(String name, int age, boolean gender, double salary, Company company) {
		super(name, age, gender);
		this.salary = salary;
		this.company = company;
	}

	public String toString() {
		return "Employee:"+super.toString()+"-"+company+"-"+salary;
	}
	
	public Company getCompany() {
		return company;
	}
	
	public double getSalary() {
		return salary;
	}
	
	public void setCompany(Company company) {
		this.company = company;
	}
	
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		DecimalFormat df = new DecimalFormat("#.#");
		String a = df.format(this.salary);
		double a1 = Double.parseDouble(a);
		String b = df.format(other.salary);
		double b1 = Double.parseDouble(b);
		if (a1 != b1)
			return false;
		return true;
	}
}



public class test6_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    Scanner sc=new Scanner(System.in);
    List<Person> personList=new ArrayList();
    String a=sc.next();
    int m;
    int n;
    while(a.equals("e")||a.equals("s")){
    	switch(a){
    	case "s":
    		Student stu=new Student(sc.next(),sc.nextInt(),sc.nextBoolean(),sc.next(),sc.next());
    		personList.add(stu);
    		a=sc.next();
    		break;
    	case "e":
    		Employee emp=new Employee(sc.next(),sc.nextInt(),sc.nextBoolean(),sc.nextDouble(), new Company(sc.next()));
    		personList.add(emp);
    		a=sc.next();
    		break;
    	default:
    		break;
    	}
    }
    Collections.sort(personList);
    for(int i=0;i<personList.size();i++){
    	System.out.println(personList.get(i));
    }
    List<Person> stuList=new ArrayList();
    List<Person> empList=new ArrayList();
    String b=sc.next();
    if(b.equals("exit"))
    	return ;
    else{
    	for(int i=0;i<personList.size();i++){
    			if(personList.get(i) instanceof Employee){
    				for(m=0;m<empList.size();m++){
    					if(personList.get(i).equals(empList.get(m)))
    						break;
    				}
    				if(m==empList.size())
						empList.add(personList.get(i));
    		    }
    			if(personList.get(i) instanceof Student){
    				for(n=0;n<stuList.size();n++){
    					if(personList.get(i).equals(stuList.get(n)))
    						break;
    				}
    				if(n==stuList.size())
						stuList.add(personList.get(i));
    			}
    	}
    }
    System.out.println("stuList");
    for(int j=0;j<stuList.size();j++){
    	System.out.println(stuList.get(j));
    }
    System.out.println("empList");
    for(int j=0;j<empList.size();j++){
    	System.out.println(empList.get(j));
    }
	}

}
