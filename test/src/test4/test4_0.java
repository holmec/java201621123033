package test4;

import java.util.ArrayList;
import java.util.Scanner;

class Goods{
	private String GoodsName;//商品名称
	private double Price;//单价
    int number;//数量
	double AllSum;   //总价
	
	public Goods(){  //创建商品信息
		this.GoodsName = GoodsName;
		this.Price = Price;
		this.number = number;
	}

	public Goods(String GoodsName,double Price,int number,double AllSum){  //创建商品信息
		this.GoodsName = GoodsName;
		this.Price = Price;
		this.number = number;
		this.AllSum=AllSum;
	}

	public String getGoodsName() {
		return GoodsName;
	}

	public void setGoodsName(String goodsName) {
		GoodsName = goodsName;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	public static void AdjustNumber(Goods[] goods,String name,int num){
		for(int i=0;i < goods.length;i++){
			if(goods[i].GoodsName.equals(name)){
				goods[i].number=num;
			}
		}
	}
	
	public static void Show(Goods[] goods) {//展示购物车信息
		
		 System.out.println("商品名\t\t单价\t\t个数\t\t总价");
		 for (int i = 0; i < goods.length; i++) {
			 if(goods[i].GoodsName!=null&&goods[i].Price!=0&&goods[i].number!=0&&goods[i].AllSum!=0) {
		System.out.println(goods[i].getGoodsName()+"\t\t"+goods[i].getPrice()+"\t\t"+goods[i].getNumber()+"\t\t"+goods[i].AllSum);
			 }
		 }
	}
	
	public void AddGoods(){//添加商品
		
	}
	
	public static void DeleteGoods(Goods[] goods,String name){//删除商品
		for (int i = 0; i < goods.length; i++) {
			if(goods[i].GoodsName.equals(name)) {
				goods[i].GoodsName=null;
				goods[i].Price=0;
				goods[i].number=0;
				goods[i].AllSum=0;
				
				
			}
		}
	}
		

}

public class test4_0 {
	
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		sc.nextLine();
		Goods[] goods = new Goods[n];
		for (int i = 0; i < goods.length; i++) {
			String GoodsName=sc.next();//商品名称
			double Price=sc.nextInt();//单价
			int number=sc.nextInt();//数量
			double AllSum=Price*number;   //总价
			Goods shop = new Goods(GoodsName,Price,number,AllSum);
			goods[i] = shop;
			
		}
		while(true){
			String str = sc.next();
			switch (str) {
			case "AdjustNum":
				String product=sc.next();
				sc.nextLine();
				int num=sc.nextInt();
				/*String a=sc.nextLine();
				int num=Integer.parseInt(a);*/
				Goods.AdjustNumber(goods,product,num);
				break;
			case "Show":
				Goods.Show(goods);
				break;
			}
		}
		
	}
	
	
}