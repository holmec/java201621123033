package test5;
import java.util.Arrays;
import java.util.Scanner;

interface IntegerStack {
	public Integer push(Integer item) ;
	public Integer pop(); 
	public Integer peek(); 
	public boolean empty(); 
	public int size(); 
}
 
 class ArrayIntegerStack implements IntegerStack{

		private Integer[] arr;
		private int top;
		public ArrayIntegerStack(int i) {
			this.setArr(new Integer[i]);
		}

		public Integer[] getArr() {
			return arr;
		}

		public void setArr(Integer[] arr) {
			this.arr = arr;
		}
		@Override
		public Integer push(Integer item) {
			if(item == null)
				return null;
			if(top == arr.length)
				return null;
			arr[top++] = item;
			return item;
		}
		@Override
		public Integer pop() {
			if(empty())
				return null;
			if (arr[top-1] == null)
				return null;
			top--;
			return arr[top];

		}
		@Override
		public Integer peek() {
			if(top == 0)
				return null;
			int num = 0;
			for (int i = 0; i < arr.length; i++) {
				if (arr[i] != null)
					num ++;
			}
			if (num != top)
				return arr[top - 1];
			for (int i = 0; i < arr.length; i++) {
				if (arr[i] == null)
					return arr[top - 1];
			}
			return arr[top - 1];
		}

		@Override
		public boolean empty() {
			if (top == 0)
				return true;
			return false;
		}
		@Override
		public int size() {
			return top;
		}
		@Override
		public String toString() {
			return  Arrays.toString(arr);
		}
 }
 
 
 
public class test5_3 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		ArrayIntegerStack stack = new ArrayIntegerStack(n);
		int m = in.nextInt();
		for (int i = 0; i < m; i++) {
			System.out.println(stack.push(in.nextInt()));
		}
		System.out.println(stack.peek() + "," + stack.empty() + ","+ stack.size());
		System.out.println(Arrays.toString(stack.getArr()));
		int x = in.nextInt();
		for (int i = 0; i < x; i++) {
			System.out.println(stack.pop());
		}
		System.out.println(stack.peek() + "," + stack.empty() + ","+ stack.size() );
		System.out.println(Arrays.toString(stack.getArr()));
	}

}