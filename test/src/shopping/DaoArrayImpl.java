package shopping;

import java.util.Arrays;

public class DaoArrayImpl implements Dao{
	
	private Goods[] shoppingList;
	private int size=80;//默认数组大小

	
	public DaoArrayImpl(int size) {
		this.size = size;
		shoppingList = new Goods[size];
	}
	
	@Override
	public void add(Goods e) {
		boolean flage = false;
		for (int i = 0; i < shoppingList.length; i++) {
			if(flage==false) {
				if(shoppingList[i]==null) {
					shoppingList[i]=e;
					flage=true;
			}
		}
		}
		
		
	}

	@Override
	public void delete(Goods e) {
		for (int i = 0; i < shoppingList.length; i++) {
			if(shoppingList[i]!=null&&shoppingList[i].getGoodsName().equals(e.getGoodsName())) {
				shoppingList[i]=shoppingList[shoppingList.length-1];
				shoppingList = Arrays.copyOf(shoppingList, shoppingList.length-1);
			}
		}
		
	}

	@Override
	public void clear() {
		for (int i = 0; i < shoppingList.length; i++) {
			shoppingList[i]=null;
		}
		 System.out.println("  系统已将购物车清空   \n\n");
	}

	@Override
	public void display() {
		System.out.print("-------------------------------------------------------------------------- \n");
		if(shoppingList.length==0){
	         System.out.println("    您的购物车是空的  \n\n");
	     }
	     else{
	    	 System.out.println("序号\t商品名\t\t单价\t\t个数\t\t总价");
			 for (int i = 0; i < shoppingList.length; i++) {
		
				 
				 if(shoppingList[i]!=null&&shoppingList[i].getGoodsName()!=null&&shoppingList[i].getPrice()!=0&&shoppingList[i].getNumber()!=0&&shoppingList[i].getAllSum()!=0) {
			System.out.println((i+1)+"\t"+shoppingList[i].getGoodsName()+"\t\t"+shoppingList[i].getPrice()+"\t\t"+shoppingList[i].getNumber()+"\t\t"+shoppingList[i].getAllSum());
				 }
			 }
			 
			 System.out.println("");
	     }
		System.out.println("------------------------------------------------------------------------ \n\n");
		
	}

	

}


