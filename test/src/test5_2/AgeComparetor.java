package test5_2;

import java.util.Comparator;

public class AgeComparetor implements Comparator<PersonSortable>{

	@Override
	public int compare(PersonSortable o1, PersonSortable o2) {
		return o1.getAge()-o2.getAge();
	}
}
