# Problems

## 1. make过程

### 问题一

```

clang: error: unable to execute command: Killed 

clang: error: linker cmmand failed due to signal (use -v to see invocation)

```

原因：swap区不够大

解决方法：
**加大分区**

`sudo fallocate -l 6G/swapfile`

`sudo chmod 600 /swapfile`

`sudo mkswap /swapfile`

`sudo swapon /swapfile`