package tset10;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class boke {
	public static void main(String[] args) {
		byte[] content = null;		
		try(FileInputStream fis = new FileInputStream("testfis.txt")){
			
			int bytesAvailabe = fis.available();//获得该文件可用的字节数
			if(bytesAvailabe>0){
			    content = new byte[bytesAvailabe];//创建可容纳文件大小的数组
			    fis.read(content);//将文件内容读入数组
			}
		}catch(FileNotFoundException e){
			System.out.println("找不到文件testfis.txt，请重新输入文件名");
		}catch(Exception e){
			System.out.println("打开或读取文件失败!");
		}
		
		System.out.println(Arrays.toString(content));//打印数组内容
	}

}
